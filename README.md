### Irrisite, a project about irrigation system management

The project's aim aim is to calculate the electrical efficiency of agricultural irrigation systems.

This has been made in 15 hours during a french hackathon called [Hack ta ferme](https://hacktaferme.fr/) between august 30 and september 1, 2019.

![application screenshot](screenshot.png "application screenshot")

## Technology

It is a fullstack Javascript program, made with NodeJS, ExpressJS, Bootstrap, Leaflet, JQuery.

## Online demonstration

You can try [the app here](https://orelab.gitlab.io/irrisite/).
Please ! Remember it has been made oin a very short time, in a hackathon :)


## Team

- Sophie Gendre
- Marie Capelle
- Aurélien Chirot

We proudly finished 5th :p
https://www.ladepeche.fr/2019/09/07/gascoagri-le-palmares-des-concours,8399463.php

