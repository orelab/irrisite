var eauDate1 = document.getElementById("eau1");
var electriciteDate1 = document.getElementById("elec1");
var eauDate2 = document.getElementById("eau2");
var electriciteDate2 = document.getElementById("elec2");
var materiel = document.getElementById("selectMateriel");
var degreComplexite = document.getElementById("selectComplexite");
var denivele = document.getElementById("denivele");
var distancePointEau = document.getElementById("distance");

var indicateur = (electriciteDate2 - electriciteDate1) / (eauDate2 - eauDate1);
var courbeCoeff = 0.0002 * indicateur + 0.4157;
var contrainteCoeff = denivele + distancePointEau;
/*
La fonction c'est courbe coeff = 0.0002*indicateur+0.4157
Coefficient de contrainte = dénivelé + distance pompe matériel
indicateur = (relevé compteur élec date 2 - relevé compteur élec date 1)/(relevé compteur eau date 2 - relevé compteur eau date 1)
*/


function diagnostic()
{
	/*
		Algo à implémenter ici :
		- true = installation optimisée
		- false = installation pouvant nécessiter des ajustements
	*/
	return false;
}

$('#myform').submit(function()
{
	if( diagnostic() )
	{
		confirm('A première vue, la consommation énergétique de votre installation est '
		+ 'dans la moyenne nationale. Nous vous proposons néanmoins d\'accéder à notre '
		+ 'plateforme d\'analyse avancée, qui devrait pouvoir optimiser mieux encore votre '
		+ 'installation.');
	}
	else
	{
		confirm('Au regard de cette première analyse, la consommation énergétique de votre '
		+ 'installation est supérieure à la moyenne des installations de même catégorie. '
		+ 'Nous vous proposons donc d\'affiner cette analyse afin de pouvoir définir ensemble '
		+ 'des leviers d\'action.');
	}

});

