/*

    If you want more informations about this project,
    please contact me at aurelien.chirot@idee-lab.fr

*/

var CO; // current selected object in toolbox
var SO; // current selected object in map

var POINTS = []; // The list of positionned objects



/*
    PLACE SEARCH BOX
*/

$("#search").on("change", function () {
    var api = "https://nominatim.openstreetmap.org/search?format=xml&q=";
    var value = $(this).val();

    $.ajax({
        url: api + value,
        dataType: "xml"
    }).done(function (xml) {
        $(xml).find('place').each(function () {
            var lat = $(this).attr("lat");
            var lng = $(this).attr("lon");

            mymap.setView([lat, lng]);
        });
    })
})



/*
    LEAFLET
*/

var mymap = L.map('mapid').setView([43.6446544, 0.5766604], 14);

L.tileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}", {
    //maxZoom: 18,
    attribution: 'Tiles &copy; Esri &mdash;',
    id: 'mapbox.streets'
}).addTo(mymap);

function addObjectClick(coords) {
    if (!CO) return;

    var mat = new Material(CO, coords.latlng);
    mat.tracePoint();
    mat.traceLine();

    POINTS.push(mat);
}
mymap.on("click", addObjectClick);



/*
    MATERIALS
*/

$.ajax("json/materials.json").done(function (data) {

    $(data).each(function (i, e) {
        $(`
            <p class="card-text">
                <a href="#" class="btn btn-primary" 
                    style="background-color:${e.gfx.color};border-color:${e.gfx.color}">
                    ${e.name}
                </a>
            </p>
        `)
            .data("data", e)
            .appendTo("#objects");
    });
});

$("#objects").delegate("p", "click", function () {
    CO = $(this).data("data");
});



/*
    MATERIAL CLASS
*/

function Material(json, coords) {

    this.getPreviousPoint = function () {
        return POINTS[POINTS.length - 1] || null;
    }

    this.name = json.name;
    this.feature = json.feature;
    this.gfx = json.gfx;
    this.coords = coords;
    this.parent = SO ? SO : this.getPreviousPoint();

    this.LLPoint;
    this.LLLine;

    SO = this;

    this.tracePoint = function () {
        var picto = L.icon({
            iconUrl: 'stylesheets/images/' + this.gfx.icon,
            iconSize: [20, 20],
            iconAnchor: [10, 10],
            popupAnchor: [0, -5]
        });

        this.LLPoint = L.marker(this.coords, {
            draggable: 'true',
            icon: picto
        }).addTo(mymap)
            .bindPopup(this.genOptions(), { minWidth: 400 });

        this.LLPoint.on('drag', function (newcoords) {
            this.coords = newcoords.latlng;
            SO = this;
            refreshAll();
        }.bind(this));

        this.LLPoint.on('popupopen', function () {
            this.LLPoint._popup.setContent(this.genOptions());

            $(this.LLPoint._popup._container).find('.btn-danger').on('click', function () {
                if (confirm("Certain ?")) {
                    this.remove();
                }
            }.bind(this));

            $(this.LLPoint._popup._container).find('.prev').on('click', function () {
                this.parent = this.parent ? this.parent.parent : POINTS[POINTS.length - 1];
                this.refresh();
            }.bind(this));

            $(this.LLPoint._popup._container).find('#parent').on('click', function (e) {
                var id = $(e.currentTarget).val();
                this.parent = POINTS[id];
                this.refresh();
            }.bind(this));
        }.bind(this));

        this.LLPoint.on('popupclose', function (e) {
            $(".leaflet-popup input,.leaflet-popup select").map(function (i, e) {
                this.feature.forEach(function (f) {
                    if (f.name == $(e).attr("name")) {
                        f.value = $(e).val();
                    }
                });
            }.bind(this));
        }.bind(this));

        this.LLPoint.on('click', function (e) {
            SO = this;
        }.bind(this));
    }

    this.traceLine = function () {
        if (!this.coords) return;
        if (!this.parent) return;
        if (!this.parent.coords) return;

        this.LLLine = new L.Polyline([this.coords, this.parent.coords], {
            color: this.gfx.color, //'#00a2ff',
            weight: 5,
            opacity: 0.9,
            smoothFactor: 1
        });
        this.LLLine.addTo(mymap);
    }

    this.refresh = function () {
        if (!this.LLLine) return;

        this.LLLine.remove();
        this.traceLine();
    }

    this.remove = function () {
        if(this.LLLine){
            this.LLLine.remove();
        }
        this.LLPoint.remove();
        this.coords = null;
        garbageCollector();
    }

    this.genOptions = function () {

        var html = `<h5 class="card-title">${this.name}</h5>`;
        var o;

        for (var i = 0; i < this.feature.length; i++) {
            o = this.feature[i];

            switch (o.type) {

                case "number":
                    html += `
                        <p class="card-text">
                            <label>${o.name}</label>
                            <input class="form-control" type="number" name="${o.name}" value="${o.value}" title="${o.unit}" />
                        </p>
                    `;
                    break;

                case "float":
                    html += `
                        <p class="card-text">
                            <label>${o.name}</label>
                            <input class="form-control" type="number" name="${o.name}" value="${o.value}" title="${o.unit}" step="0.01" />
                        </p>
                    `;
                    break;

                case "select":
                    html += `
                        <p class="card-text">
                            <label>${o.name}</label>
                            <select name="${o.name}" class="form-control">`;

                    for (var j = 0; j < o.list.length; j++) {
                        html += "<option" + ( o.value==o.list[j]?" selected":"" ) + `>${o.list[j]}</option>`;
                    }

                    html += `</select></p>`;
                    break;

                case "string":
                    html += `
                        <p class="card-text">
                            <label>${o.name}</label>
                            <input class="form-control" type="text" name="${o.name}" value="${o.value||''}" title="${o.unit}" />
                        </p>
                    `;
                    break;
            }
        }

        html += `
            <p class="card-text">
                <label>raccordement</label>
                <br/>
            <button class="btn btn-primary prev"><</button>
                <select id="parent" class="form-control">`;

        for (var i = 0; i < POINTS.length; i++) {
            html += `<option value="${i}">${POINTS[i].name}</option>`;
        }
        html += `</select>
            <button class="btn btn-primary next">></button>
            <button class="btn btn-danger">suppr</button>
            </p>
        `;

        return html;
    }
}


/*
    MISC
*/

function refreshAll() {
    for (var i = 0; i < POINTS.length; i++) {
        POINTS[i].refresh();
    }
}


function garbageCollector() {
    for (var i = 0; i < POINTS.length; i++) {
        if (!POINTS[i].coords) {
            POINTS.splice(i, 1);
        }
    }
}




/*
    ESCAPE BUTTON
*/
$('body').keyup(function(e){
    if(e.key === "Escape"){
        mymap.closePopup();
    }
});



/*
    DOWNLOAD BUTTON
*/

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

$(".nav-link").on("click", function () {
    var liste = [];

    for(var i=0 ; i<POINTS.length ; i++){
        liste.push(POINTS[i].feature);
    }
    download("projet.json", JSON.stringify(liste));
});